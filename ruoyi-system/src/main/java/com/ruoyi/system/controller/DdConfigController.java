package com.ruoyi.system.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DdConfig;
import com.ruoyi.system.service.IDdConfigService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 钉钉配置Controller
 * 
 * @author xx
 * @date 2021-04-21
 */
@RestController
@RequestMapping("/system/ddConfig")
public class DdConfigController extends BaseController
{
    @Autowired
    private IDdConfigService ddConfigService;

    /**
     * 查询钉钉配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:ddConfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(DdConfig ddConfig)
    {
        startPage();
        List<DdConfig> list = ddConfigService.selectDdConfigList(ddConfig);
        return getDataTable(list);
    }

    /**
     * 导出钉钉配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:ddConfig:export')")
    @Log(title = "钉钉配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DdConfig ddConfig)
    {
        List<DdConfig> list = ddConfigService.selectDdConfigList(ddConfig);
        ExcelUtil<DdConfig> util = new ExcelUtil<DdConfig>(DdConfig.class);
        return util.exportExcel(list, "ddConfig");
    }

    /**
     * 获取钉钉配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:ddConfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(ddConfigService.selectDdConfigById(id));
    }

    /**
     * 新增钉钉配置
     */
    @PreAuthorize("@ss.hasPermi('system:ddConfig:add')")
    @Log(title = "钉钉配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DdConfig ddConfig)
    {
        return toAjax(ddConfigService.insertDdConfig(ddConfig));
    }

    /**
     * 修改钉钉配置
     */
    @PreAuthorize("@ss.hasPermi('system:ddConfig:edit')")
    @Log(title = "钉钉配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DdConfig ddConfig)
    {
        return toAjax(ddConfigService.updateDdConfig(ddConfig));
    }

    /**
     * 删除钉钉配置
     */
    @PreAuthorize("@ss.hasPermi('system:ddConfig:remove')")
    @Log(title = "钉钉配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(ddConfigService.deleteDdConfigByIds(ids));
    }
}
