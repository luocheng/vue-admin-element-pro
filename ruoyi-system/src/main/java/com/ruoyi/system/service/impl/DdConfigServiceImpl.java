package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DdConfigMapper;
import com.ruoyi.system.domain.DdConfig;
import com.ruoyi.system.service.IDdConfigService;

/**
 * 钉钉配置Service业务层处理
 * 
 * @author xx
 * @date 2021-04-21
 */
@Service
public class DdConfigServiceImpl extends ServiceImpl<DdConfigMapper, DdConfig> implements IDdConfigService
{
    @Autowired
    private DdConfigMapper ddConfigMapper;

    /**
     * 查询钉钉配置
     * 
     * @param id 钉钉配置ID
     * @return 钉钉配置
     */
    @Override
    public DdConfig selectDdConfigById(String id)
    {
    	return this.getById(id);
    }

    /**
     * 查询钉钉配置列表
     * 
     * @param ddConfig 钉钉配置
     * @return 钉钉配置
     */
    @Override
    public List<DdConfig> selectDdConfigList(DdConfig ddConfig)
    {
        return ddConfigMapper.selectDdConfigList(ddConfig);
    }

    /**
     * 新增钉钉配置
     * 
     * @param ddConfig 钉钉配置
     * @return 结果
     */
    @Override
    public boolean insertDdConfig(DdConfig ddConfig)
    {
        ddConfig.setCreateTime(DateUtils.getNowDate());
        return this.save(ddConfig);
    }

    /**
     * 修改钉钉配置
     * 
     * @param ddConfig 钉钉配置
     * @return 结果
     */
    @Override
    public boolean updateDdConfig(DdConfig ddConfig)
    {
        ddConfig.setUpdateTime(DateUtils.getNowDate());
        return updateById(ddConfig);
    }

    /**
     * 批量删除钉钉配置
     * 
     * @param ids 需要删除的钉钉配置ID
     * @return 结果
     */
    @Override
    public int deleteDdConfigByIds(String[] ids)
    {
        return ddConfigMapper.deleteDdConfigByIds(ids);
    }

    /**
     * 删除钉钉配置信息
     * 
     * @param id 钉钉配置ID
     * @return 结果
     */
    @Override
    public boolean deleteDdConfigById(String id)
    {
        return this.removeById(id);
    }
}
