package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.TestTable;

/**
 * 用户测试Service接口
 * 
 * @author ruoyi
 * @date 2021-03-24
 */
public interface ITestTableService extends IService<TestTable>
{
    /**
     * 查询用户测试
     * 
     * @param id 用户测试ID
     * @return 用户测试
     */
    public TestTable selectTestTableById(String id);

    /**
     * 查询用户测试列表
     * 
     * @param testTable 用户测试
     * @return 用户测试集合
     */
    public List<TestTable> selectTestTableList(TestTable testTable);

    /**
     * 新增用户测试
     * 
     * @param testTable 用户测试
     * @return 结果
     */
    public boolean insertTestTable(TestTable testTable);

    /**
     * 修改用户测试
     * 
     * @param testTable 用户测试
     * @return 结果
     */
    public boolean updateTestTable(TestTable testTable);

    /**
     * 批量删除用户测试
     * 
     * @param ids 需要删除的用户测试ID
     * @return 结果
     */
    public int deleteTestTableByIds(String[] ids);

    /**
     * 删除用户测试信息
     * 
     * @param id 用户测试ID
     * @return 结果
     */
    public boolean deleteTestTableById(String id);
}
