package com.ruoyi.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TestTableMapper;
import com.ruoyi.system.domain.TestTable;
import com.ruoyi.system.service.ITestTableService;

/**
 * 用户测试Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-03-24
 */
@Service
public class TestTableServiceImpl extends ServiceImpl<TestTableMapper, TestTable> implements ITestTableService
{
    @Autowired
    private TestTableMapper testTableMapper;

    /**
     * 查询用户测试
     * 
     * @param id 用户测试ID
     * @return 用户测试
     */
    @Override
    public TestTable selectTestTableById(String id)
    {
    	return this.getById(id);
    }

    /**
     * 查询用户测试列表
     * 
     * @param testTable 用户测试
     * @return 用户测试
     */
    @Override
    public List<TestTable> selectTestTableList(TestTable testTable)
    {
        return testTableMapper.selectTestTableList(testTable);
    }

    /**
     * 新增用户测试
     * 
     * @param testTable 用户测试
     * @return 结果
     */
    @Override
    public boolean insertTestTable(TestTable testTable)
    {
    	boolean flag = this.save(testTable);
        return flag;
    }

    /**
     * 修改用户测试
     * 
     * @param testTable 用户测试
     * @return 结果
     */
    @Override
    public boolean updateTestTable(TestTable testTable)
    {
        return updateById(testTable);
    }

    /**
     * 批量删除用户测试
     * 
     * @param ids 需要删除的用户测试ID
     * @return 结果
     */
    @Override
    public int deleteTestTableByIds(String[] ids)
    {
        return testTableMapper.deleteTestTableByIds(ids);
    }

    /**
     * 删除用户测试信息
     * 
     * @param id 用户测试ID
     * @return 结果
     */
    @Override
    public boolean deleteTestTableById(String id)
    {
        return this.removeById(id);
    }
}
