package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.TestTable;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2021-03-23
 */
public interface TestTableMapper extends BaseMapper<TestTable>
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public TestTable selectTestTableById(String id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param testTable 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TestTable> selectTestTableList(TestTable testTable);

    /**
     * 新增【请填写功能名称】
     * 
     * @param testTable 【请填写功能名称】
     * @return 结果
     */
    public int insertTestTable(TestTable testTable);

    /**
     * 修改【请填写功能名称】
     * 
     * @param testTable 【请填写功能名称】
     * @return 结果
     */
    public int updateTestTable(TestTable testTable);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteTestTableById(String id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTestTableByIds(String[] ids);
}
