package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.DdConfig;

/**
 * 钉钉配置Mapper接口
 * 
 * @author xx
 * @date 2021-04-21
 */
public interface DdConfigMapper extends BaseMapper<DdConfig>
{
    /**
     * 查询钉钉配置
     * 
     * @param id 钉钉配置ID
     * @return 钉钉配置
     */
    public DdConfig selectDdConfigById(String id);

    /**
     * 查询钉钉配置列表
     * 
     * @param ddConfig 钉钉配置
     * @return 钉钉配置集合
     */
    public List<DdConfig> selectDdConfigList(DdConfig ddConfig);

    /**
     * 新增钉钉配置
     * 
     * @param ddConfig 钉钉配置
     * @return 结果
     */
    public int insertDdConfig(DdConfig ddConfig);

    /**
     * 修改钉钉配置
     * 
     * @param ddConfig 钉钉配置
     * @return 结果
     */
    public int updateDdConfig(DdConfig ddConfig);

    /**
     * 删除钉钉配置
     * 
     * @param id 钉钉配置ID
     * @return 结果
     */
    public int deleteDdConfigById(String id);

    /**
     * 批量删除钉钉配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDdConfigByIds(String[] ids);
}
