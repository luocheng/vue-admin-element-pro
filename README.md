<p align="center">
  <img width="320" src="https://wpimg.wallstcn.com/ecc53a42-d79b-42e2-8852-5126b810a4c8.svg">
</p>

<p align="center">
  <a href="https://github.com/vuejs/vue">
    <img src="https://img.shields.io/badge/vue-v2.6.12-brightgreen" alt="vue">
  </a>
  <a href="https://github.com/ElemeFE/element">
    <img src="https://img.shields.io/badge/element--ui-v2.5.1-blue" alt="element-ui">
  </a>
  <a href="https://github.com/apache/echarts">
    <img src="https://img.shields.io/badge/echarts-v5.1.1-yellow" alt="echarts">
  </a>
  <a href="https://github.com/PanJiaChen/vue-element-admin/blob/master/LICENSE">
    <img src="https://img.shields.io/badge/vue--cli-v4.4.6-brightgreen" alt="vue-cli">
  </a>
  <a href="https://github.com/PanJiaChen/vue-element-admin/releases">
    <img src="https://img.shields.io/badge/release-v1.0.0-blue" alt="GitHub release">
  </a>
</p>

## 简介

> vue-admin-element-pro 基于vue和element-ui实现，在ruoyi-vue的基础上对界面进行全方位的优化调整，符合当前主流后台管理界面的排版样式。同时将echarts版本从v4.9.0升级至v5.1.1，基础样式更加美观，功能更加全面。

- [在线预览](http://vue-admin-element-pro.com)

## 新增功能

- echart v4.9.0升级至v5.1.1，echart引入方式改为5.0版本。
- 新增echart配色：暗黑、明亮（跟随系统侧边栏主题）。
- 新增骨架屏、空状态、小圆点、结果等ui组件。
- 修改401、404页面，适配移动端。
- 修改字典获取方式，封装el-select、el-radio组件，免去前端调用字典需要写一个请求方法的窘境。
- 代码优化，界面排版样式调整。

## 内置功能

> vue-admin-element-pro并未对ruoyi-vue基础功能进行修改。

- 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
- 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
- 岗位管理：配置系统用户所属担任职务。
- 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
- 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
- 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
- 参数管理：对系统动态配置常用参数。
- 通知公告：系统通知公告信息发布维护。
- 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
- 登录日志：系统登录日志记录查询包含登录异常。
- 在线用户：当前系统中活跃用户状态监控。
- 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
- 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
- 系统接口：根据业务代码自动生成相关的api接口文档。
- 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
- 缓存监控：对系统的缓存信息查询，命令统计等。
- 在线构建器：拖动表单元素生成相应的HTML代码。
- 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## 鸣谢

> Free & Open Source

<p align="left">
  <a href="https://cn.vuejs.org">
     <img src="https://z3.ax1x.com/2021/05/26/2pQLlT.png" alt="vue">
  </a>
  <a href="https://element.eleme.cn/#/zh-CN">
     <img src="https://z3.ax1x.com/2021/05/26/2pQO6U.png" alt="element-ui">
  </a>
     <a href="http://www.ruoyi.vip">
    <img src="https://z3.ax1x.com/2021/05/26/2plJBQ.png" alt="ruoyi-vue">
  </a>
</p>


## 贡献者（排名不分先后）

> 感谢以下成员对vue-admin-element-pro做出的贡献！

<p align="left">
  <a href="https://gitee.com/xuanyuanyanger">
    <img width="50" src="https://z3.ax1x.com/2021/05/26/2pnAC8.jpg" alt="vue">
  </a>
  <a href="https://gitee.com/Advictoramn1">
    <img width="50" src="https://z3.ax1x.com/2021/05/26/2pnG2F.jpg" alt="element-ui">
  </a>
</p>

## 效果图

<table>
    <tr>
        <td><img src="https://z3.ax1x.com/2021/05/26/2pZiNT.png"/></td>
        <td><img src="https://z3.ax1x.com/2021/05/26/2pZF4U.png"/></td>
    </tr>
    <tr>
        <td><img src="https://z3.ax1x.com/2021/05/26/2pZE34.png"/></td>
        <td><img src="https://z3.ax1x.com/2021/05/26/2pZACF.png"/></td>
    </tr>
    <tr>
        <td><img src="https://z3.ax1x.com/2021/05/26/2pZVgJ.png"/></td>
        <td><img src="https://z3.ax1x.com/2021/05/26/2pZZv9.png"/></td>
    </tr>
	<tr>
        <td><img src="https://z3.ax1x.com/2021/05/26/2pZPEV.png"/></td>
        <td><img src="https://z3.ax1x.com/2021/05/26/2peEIf.png"/></td>
    </tr>
</table>
