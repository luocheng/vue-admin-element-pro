package com.baomidou.ant.test.service.impl;

import com.baomidou.ant.test.entity.Table;
import com.baomidou.ant.test.mapper.TableMapper;
import com.baomidou.ant.test.service.ITableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2021-03-23
 */
@Service
public class TableServiceImpl extends ServiceImpl<TableMapper, Table> implements ITableService {

}
