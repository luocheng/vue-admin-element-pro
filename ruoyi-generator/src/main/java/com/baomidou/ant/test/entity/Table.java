package com.baomidou.ant.test.entity;

import java.math.BigDecimal;
import com.ruoyi.common.core.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2021-03-23
 */
@TableName("test_table")
public class Table extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private String userName;

    private Integer age;

    private Integer sex;

    private BigDecimal amount;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Table{" +
            "userName=" + userName +
            ", age=" + age +
            ", sex=" + sex +
            ", amount=" + amount +
        "}";
    }
}
