package com.baomidou.ant.test.service;

import com.baomidou.ant.test.entity.Table;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2021-03-23
 */
public interface ITableService extends IService<Table> {

}
