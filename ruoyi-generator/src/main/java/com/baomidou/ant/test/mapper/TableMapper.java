package com.baomidou.ant.test.mapper;

import com.baomidou.ant.test.entity.Table;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2021-03-23
 */
public interface TableMapper extends BaseMapper<Table> {

}
