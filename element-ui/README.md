## 开发

```bash
# 克隆项目
git clone https://gitee.com/y_project/RuoYi-Vue

# 进入项目目录
cd ruoyi-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 目录结构

```bash
src                               源码目录
|-- api                              接口
|-- assets                           静态资源
|-- |-- 401_images					          
|-- |-- 404_images					         
|-- |-- icons					                
|-- |-- images					              
|-- |-- logo					              
|-- |-- styles					              
|-- components                       公用组件，全局文件
|-- directive                        自定义指令，v-directive
|-- layout                           页面布局
|-- router                           路由
|-- store                            vuex
|-- utils                            自定义方法
|-- lib                              外部引用的插件存放及修改文件
|-- views                         视图目录
|   |-- system                           一级菜单（模块）
|   |-- |-- user                         二级菜单（页面）
|   |-- |-- |-- mixins                   模块混入的公共数据
|   |-- |-- |-- |-- index.js                   
|   |-- |-- |-- modules                  表单、抽屉组件
|   |-- |-- |-- |-- userInfoDrawer.vue          
|   |-- |-- |-- index.vue                模块入口页面 
```