import T from './SelectDictTag'

const SelectDictTag = {
  install: (Vue) => {
    Vue.component('SelectDictTag', T)
  }
}

export default SelectDictTag
