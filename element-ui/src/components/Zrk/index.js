import T from './ZrkDictSelectTag'

const ZrkDictSelectTag = {
  install: (Vue) => {
    Vue.component('ZrkDictSelectTag', T)
  }
}

export default ZrkDictSelectTag
