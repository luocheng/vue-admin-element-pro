import T from './SelectMultiTag'

const SelectMultiTag = {
  install: (Vue) => {
    Vue.component('SelectMultiTag', T)
  }
}

export default SelectMultiTag
