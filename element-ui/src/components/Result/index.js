import T from './src/index'

const ElResult = {
  install: (Vue) => {
    Vue.component('ElResult', T)
  }
}

export default ElResult
