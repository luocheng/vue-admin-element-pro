import ElDot from './index.vue'

export default {
  install: Vue => {
    Vue.component('el-dot', ElDot)
  }
}
