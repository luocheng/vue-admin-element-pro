import Vue from 'vue'

import Cookies from 'js-cookie'

import 'default-passive-events'

import Element from 'element-ui'
import './assets/styles/element-variables.scss'

import '@/assets/styles/index.scss' // global css
import '@/assets/styles/ruoyi.scss' // ruoyi css
import App from './App'
import store from './store'
import router from './router'
// El-dot 小圆点
import ElDot from '@/components/Dot'
// El-result 结果
import ElResult from '@/components/Result'
import SelectMultiTag from '@/components/Zrk/SelectMultiTag'
import SelectDictTag from '@/components/Zrk/SelectDictTag'
import permission from '@/directive/permission'
import waves from '@/directive/waves' // 水波纹指令

import './assets/icons' // icon
import './permission' // permission control
import { getDicts } from '@/api/system/dict/data'
import { getConfigKey } from '@/api/system/config'
import { parseTime, resetForm, addDateRange, selectDictLabel, selectDictLabels, download, handleTree } from '@/utils/ruoyi'
import Pagination from '@/components/Pagination'

// 自定义表格工具扩展
import RightToolbar from '@/components/RightToolbar'
// 可拖拽、全屏、最小化 Dialog
import ZrkDragDialog from '@/components/Zrk/ZrkDragDialog'
// 上传文件弹框 Upload
import ZrkUpload from '@/components/Zrk/ZrkUpload'
// El-empty 缺省图
import ElEmpty from '@/components/Empty'
// El-skeleton 骨架屏
import ElSkeleton from '@/components/Skeleton'
import ElSkeletonItem from '@/components/Skeleton-item'


// 全局方法挂载
Vue.prototype.getDicts = getDicts
Vue.prototype.getConfigKey = getConfigKey
Vue.prototype.parseTime = parseTime
Vue.prototype.resetForm = resetForm
Vue.prototype.addDateRange = addDateRange
Vue.prototype.selectDictLabel = selectDictLabel
Vue.prototype.selectDictLabels = selectDictLabels
Vue.prototype.download = download
Vue.prototype.handleTree = handleTree

Vue.prototype.msgSuccess = function (msg) {
  this.$message({ showClose: true, message: msg, type: "success" })
}

Vue.prototype.msgError = function (msg) {
  this.$message({ showClose: true, message: msg, type: "error" })
}

Vue.prototype.msgInfo = function (msg) {
  this.$message.info(msg)
}

// 全局组件挂载
Vue.component('Pagination', Pagination)
Vue.component('RightToolbar', RightToolbar)
Vue.component('zrk-drag-dialog', ZrkDragDialog)
Vue.component('zrk-upload', ZrkUpload)
Vue.component('el-empty', ElEmpty)
Vue.component('el-skeleton', ElSkeleton)
Vue.component('el-skeleton-item', ElSkeletonItem)

Vue.use(ElDot)
Vue.use(ElResult)
Vue.use(permission)
Vue.use(waves)
Vue.use(SelectMultiTag)
Vue.use(SelectDictTag)

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */

Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
