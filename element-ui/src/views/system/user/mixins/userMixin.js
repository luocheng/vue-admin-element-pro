import { treeselect } from '@/api/system/dept'

export const userMixin = {
  data() {
    return {
      // 状态数据字典
      statusOptions: [],
      // 性别状态字典
      sexOptions: [],
      // 部门树选项
      deptOptions: [],
      // 默认密码
      initPassword: undefined,
    }
  },
  // 判断是开发or生产环境
  computed: {
    process() {
      return process.env.VUE_APP_BASE_API
    }
  },
  created() {
    this.getTreeselect()
    // 获取默认密码
    this.getConfigKey('sys.user.initPassword').then((response) => {
      this.initPassword = response.msg
    })
    // 获取状态字典
    this.getDicts('sys_normal_disable').then((response) => {
      this.statusOptions = response.data
    })
    // 获取性别字典
    this.getDicts('sys_user_sex').then((response) => {
      this.sexOptions = response.data
    })
  },
  methods: {
    // 用户状态字典翻译
    statusFormat(row, column) {
      return this.selectDictLabel(this.statusOptions, row.status)
    },
    /** 查询部门下拉树结构 */
    getTreeselect() {
      treeselect().then((response) => {
        this.deptOptions = response.data
      })
    },
    /** 修复Element-UI Drawer 鼠标点击移动至遮罩层自动关闭的问题  */
    handleWrapperMousedown(e) {
      // 如果为true，则表示点击发生在遮罩层
      this.classmodel = !!e.target.classList.contains('el-drawer__container')
    },
    handleWrapperMouseup(e) {
      if (!!e.target.classList.contains('el-drawer__container') && this.classmodel) {
        this.handleClose()
      }
    },
  }
}
