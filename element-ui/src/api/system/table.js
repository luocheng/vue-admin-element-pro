import request from '@/utils/request'

// 查询用户测试列表
export function listTable(query) {
  return request({
    url: '/system/table/list',
    method: 'get',
    params: query
  })
}

// 查询用户测试详细
export function getTable(id) {
  return request({
    url: '/system/table/' + id,
    method: 'get'
  })
}

// 新增用户测试
export function addTable(data) {
  return request({
    url: '/system/table',
    method: 'post',
    data: data
  })
}

// 修改用户测试
export function updateTable(data) {
  return request({
    url: '/system/table',
    method: 'put',
    data: data
  })
}

// 删除用户测试
export function delTable(id) {
  return request({
    url: '/system/table/' + id,
    method: 'delete'
  })
}

// 导出用户测试
export function exportTable(query) {
  return request({
    url: '/system/table/export',
    method: 'get',
    params: query
  })
}