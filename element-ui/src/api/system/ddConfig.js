import request from '@/utils/request'

// 查询钉钉配置列表
export function listDdConfig(query) {
  return request({
    url: '/system/ddConfig/list',
    method: 'get',
    params: query
  })
}

// 查询钉钉配置详细
export function getDdConfig(id) {
  return request({
    url: '/system/ddConfig/' + id,
    method: 'get'
  })
}

// 新增钉钉配置
export function addDdConfig(data) {
  return request({
    url: '/system/ddConfig',
    method: 'post',
    data: data
  })
}

// 修改钉钉配置
export function updateDdConfig(data) {
  return request({
    url: '/system/ddConfig',
    method: 'put',
    data: data
  })
}

// 删除钉钉配置
export function delDdConfig(id) {
  return request({
    url: '/system/ddConfig/' + id,
    method: 'delete'
  })
}

// 导出钉钉配置
export function exportDdConfig(query) {
  return request({
    url: '/system/ddConfig/export',
    method: 'get',
    params: query
  })
}