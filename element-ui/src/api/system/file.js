/**
 * 导入文件
 */
import request from '@/utils/request'

// 下载导入模板
export function importTemplate(url) {
  return request({
    url,
    method: 'get'
  })
}
