/**
 * 公海接口
 */
import request from '@/utils/request'

export function listGH(query) {
  return request({
    url: '/customer/gh/list',
    method: 'get',
    params: query
  })
}
